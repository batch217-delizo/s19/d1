console.log('hellow world');

/* 
      Conditional Statement
    
       --> If,  else clause
*/


let numG = -1;

numG = -10;

if(numG < 0) {
  console.log('Hello');
}

if(numG < 0) {
  console.log('Hello');
}


let numH = 1;


if(numG > 0) {
  console.log('Hello');
} else if(numH > 0) {
  console.log('World');
} 


// else statement
if(numG > 0) {
  console.log('Hello');
} else if(numH > 0) {
  console.log('World');
} else {
  console.log("Again");
}

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if(windSpeed < 30){
        return "Not a typhoon yes.";
    } else if(windSpeed <= 61){
        return "Tropical depression detected.";
    } else if( windSpeed >= 62 && windSpeed <= 88) {
        return "Tropical storm detected.";
    } else if(windSpeed >= 89 && windSpeed <= 177) {
        return "Severe tropical storm detected"; 
    } else {
        return "Typhoon detected";
    }
}


message = determineTyphoonIntensity(69);
console.log(message);

message = determineTyphoonIntensity(27);
console.log(message);

message = determineTyphoonIntensity(200);
console.log(message);

message = determineTyphoonIntensity(100);
console.log(message);

message = determineTyphoonIntensity(178);
console.log(message);

if(message == "Tropical storm detected.") {
  console.warn(message);
}

message = determineTyphoonIntensity(75);
console.log(message);

if(message == "Tropical storm detected.") {
  console.warn(message);
}

//truthy example
if(1) {
  console.log('Truthy');
}

if(false) {
  console.log("Falsy")
}

if(0) {
  console.log("Falsy");
}


if(undefined) {
  console.log("Falsy");
}



//Section Conditional Ternary Operator

let ternaryResult = (1 < -1) ? true : false;  
console.log("Result of Ternary Operator: " + ternaryResult);

//Multiple statement execution
let name;

function isOfLegalAge() {
  name = "John";

  return "You are of legal the age limit"
}

function isUnderAge() {
  name = "Jane";

  return "You are of under the age limit"
}

// let age = parseInt(prompt("What is your age?"))
// //  parseInt --> converts a string number into an Integer
// console.log(age)


// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator in function: " + legalAge + ", " + name);

// //[Section] Switch Statement
// let day = prompt('What a day of the week is it today?').toLocaleLowerCase();
// console.log(day);


// switch (day) {
//   case "monday":
//      console.log('The color of the day is red');
//      break;
  
//   case "tuesday":
//      console.log("The color of the day is orange");
//      break;
//   case "wednesday":
//      console.log("The color of the day is yellow");
//      break;
//   case "thursday":
//      console.log("The color of the day is green");   
//      break;
//   case "friday":
//      console.log("The color of the day is blue");  
//      break;
//   case "saturday":
//      console.log("The color of the day is indigo");    
//      break;
//   case "sunday":
//      console.log("The color of the day is violet");    
//      break;
//   default:
//      console.log("Invalid input day.")
//      break; 
    
// }


// section Try-Catch-Finally Statement
function showIntensityAlert(windSpeed){
    try {
      //attempt to execute a code
      alert(determineTyphoonIntensity(windSpeed));
    } catch(error) {
      console.log(typeof error);
      console.warn(error.message);
    } finally {
      alert('Intensity updates will show new alert.');
    }     
}

showIntensityAlert(56);



